import 'package:flutter/material.dart';
import 'package:flutter_sms_inbox/flutter_sms_inbox.dart';
import 'package:permission_handler/permission_handler.dart';
import 'widgets.dart';
import 'toggle_view.dart';
import 'sms_query_button.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final SmsQuery _query = SmsQuery();
  List<SmsMessage> _messages = [];
  int itemsRetrieved = 5; // Items to retrieve
  bool toggle = true; // toggle if only phone numbers or full messages

  String? testContact(int index) {
    if (index == 1) {
      return '6822391122';
    } else if (index == 2) {
      return '+19729832532';
    } else {
      return null;
    }
  } // still a work in progress

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Text Inbox App'),
          backgroundColor: Color(0xFF06062A),
          shadowColor: Colors.black,
        ),
        body: Column(
          children: [
            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(height: 10),
                  Text(
                    "Number of Messages Retrieved",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    itemsRetrieved.toString(),
                    style: TextStyle(
                      fontSize: 36,
                      fontWeight: FontWeight.w900,
                    ),
                  ), // Num of messages retrieved
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                        onPressed: () {
                          setState(() {
                            itemsRetrieved++;
                          });
                        },
                        style: customButton(),
                        child: Icon(
                          Icons.add,
                          size: 30,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      TextButton(
                        onPressed: () {
                          setState(() {
                            itemsRetrieved--;
                          });
                        },
                        style: customButton(),
                        child: Icon(
                          Icons.remove,
                          size: 30,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    "Clear Inbox",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        _messages = [];
                      });
                    },
                    style: customButton(),
                    child: Icon(
                      Icons.remove_from_queue_outlined,
                      size: 30,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        toggle = !toggle;
                      });
                    },
                    child: Text(
                      "Phone Numbers Only: ${toggle}",
                    ),
                    style: customButton(),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 5,
              child: toggleView(toggle, _messages),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          // Performs query to grab inbox info
          backgroundColor: Color(0xFF1A355F),
          onPressed: () async {
            var permission = await Permission.sms.status;
            if (permission.isGranted) {
              final messages = await _query.querySms(
                // can change the kind of query to perform
                // alternatives : SmsQueryKind.draft to get drafted messages
                kinds: [SmsQueryKind.inbox, SmsQueryKind.sent],
                // TODO: Make it so the user can choose who to get messages from
                address: testContact(3),
                count: itemsRetrieved,
              );
              setState(() {
                _messages = messages;
                //TODO: saveContactNumbers(messages);
              });
            } else {
              await Permission.sms.request();
            }
          },
          child: const Icon(Icons.refresh),
        ),
      ),
    );
  }
}
