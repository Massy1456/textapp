import 'package:flutter/material.dart';
import 'package:flutter_sms_inbox/flutter_sms_inbox.dart';
import 'package:permission_handler/permission_handler.dart';
import 'widgets.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final SmsQuery _query = SmsQuery();
  List<SmsMessage> _messages = [];
  int itemsRetrieved = 5;
  bool toggle = true;

  Container toggleView(bool choice) {
    if (!choice) {
      return Container(
        padding: const EdgeInsets.all(10.0),
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: _messages.length,
          itemBuilder: (BuildContext context, int i) {
            var message = _messages[i];

            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                title: Text(
                  // We are getting the contact numbers from a hashmap that we created, so we can store all
                  // the hash:numbers of each person
                  '${message.sender}',
                  style: TextStyle(fontWeight: FontWeight.w800),
                ),
                subtitle: Text(
                  '${message.dateSent} \n ${message.body}',
                  style: TextStyle(fontWeight: FontWeight.w700),
                ),
                tileColor: Color(0xFFF0EBBC),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
              ),
            );
          },
        ),
      );
    } else {
      return Container(
        padding: const EdgeInsets.all(10.0),
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: _messages.length,
          itemBuilder: (BuildContext context, int i) {
            var message = _messages[i];

            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                title: Center(
                  child: Text(
                    // We are getting the contact numbers from a hashmap that we created, so we can store all
                    // the hash:numbers of each person
                    '${message.sender}',
                    style: TextStyle(fontWeight: FontWeight.w900),
                  ),
                ),
                tileColor: Color(0xFFF0EBBC),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
            );
          },
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Project Mango'),
          backgroundColor: Colors.deepOrangeAccent,
          shadowColor: Colors.black,
        ),
        body: Column(
          children: [
            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(height: 10),
                  Text(
                    "Number of Messages Retrieved",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    itemsRetrieved.toString(),
                    style: TextStyle(
                      fontSize: 36,
                      fontWeight: FontWeight.w900,
                    ),
                  ), // Num of messages retrieved
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                        onPressed: () {
                          setState(() {
                            itemsRetrieved++;
                          });
                        },
                        style: customButton(),
                        child: Icon(
                          Icons.add,
                          size: 30,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      TextButton(
                        onPressed: () {
                          setState(() {
                            itemsRetrieved--;
                          });
                        },
                        style: customButton(),
                        child: Icon(
                          Icons.remove,
                          size: 30,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    "Clear Inbox",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        _messages = [];
                      });
                    },
                    style: customButton(),
                    child: Icon(
                      Icons.remove_from_queue_outlined,
                      size: 30,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        toggle = !toggle;
                      });
                    },
                    child: Text(
                      "Phone Numbers Only: ${toggle}",
                    ),
                    style: customButton(),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 5,
              child: toggleView(toggle),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.deepOrangeAccent,
          onPressed: () async {
            var permission = await Permission.sms.status;
            if (permission.isGranted) {
              final messages = await _query.querySms(
                kinds: [SmsQueryKind.inbox, SmsQueryKind.sent],
                // address: ,
                count: itemsRetrieved,
              );
              debugPrint('sms inbox messages: ${messages.length}');
              setState(() {
                _messages = messages;
                // saveContactNumbers();
              });
            } else {
              await Permission.sms.request();
            }
          },
          child: const Icon(Icons.refresh),
        ),
      ),
    );
  }
}
